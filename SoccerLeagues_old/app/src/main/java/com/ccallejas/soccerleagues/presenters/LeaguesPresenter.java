package com.ccallejas.soccerleagues.presenters;

import com.ccallejas.soccerleagues.models.Leagues;
import com.ccallejas.soccerleagues.models.Listleagues;
import com.ccallejas.soccerleagues.services.Repository;
import com.ccallejas.soccerleagues.views.interfaces.IActivityView;
import java.io.IOException;
import java.util.ArrayList;

public class LeaguesPresenter extends  BasePresenter <IActivityView>{

    private Repository repository;

    public void getListLigas() {
        if (getValidateInternet().isConnected()) {
            repository = new Repository();
            createThreadGetLigas();
        } else {
            getView().showAlert();
        }
    }

    public void createThreadGetLigas(){
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                getLigas();
            }
        });
        thread.start();
    }


    private void getLigas() {
        try {
            Listleagues listligas = repository.getLeagues();
            getView().getListLeagues(filtrarLigas(listligas));
        } catch (final IOException e) {
            e.printStackTrace();
        }

    }

    private Listleagues filtrarLigas(Listleagues listligas) {
        Listleagues ligas = new Listleagues();
        ArrayList<Leagues> listaLeagues = new ArrayList<>();

        if (listligas != null) {
            for (Leagues leagues : listligas.getListaLeagues()) {

                if ("Soccer".equals(leagues.getStrSport())) {
                    listaLeagues.add(leagues);
                }
            }
            ligas.setListaLeagues(listaLeagues);
        }

        return ligas;
    }
}
