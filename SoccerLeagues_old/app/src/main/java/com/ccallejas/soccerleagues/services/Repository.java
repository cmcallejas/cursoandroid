package com.ccallejas.soccerleagues.services;


import android.util.Log;

import com.ccallejas.soccerleagues.models.Listleagues;
import com.ccallejas.soccerleagues.models.TeamsList;
import java.io.IOException;
import retrofit2.Call;
import retrofit2.Response;


public class Repository {

    private IServices iServices;

    public Repository() {
        ServicesFactory servicesFactory = new ServicesFactory();
        iServices = (IServices) servicesFactory.getInstanceService(IServices.class);
    }

    public Listleagues getLeagues() throws IOException{
        try{
            Call<Listleagues> call =  iServices.getLeagues();
            System.err.println("CALL 1 "+call.toString());
            Response<Listleagues> response = call.execute();
            System.err.println("CALL "+call.toString());
            if (response.errorBody() != null){
                throw  defaultError();
            }else{
                return response.body();
            }
        }catch(IOException e){
            throw defaultError();
        }

    }

    public TeamsList getEquipos(String idLeague) throws IOException {
        try {
            //Call<TeamsList> call =  iServices.getListTeams(idLeague);
            Call<TeamsList> call = iServices.getListTeams();
            Log.i("CALL 1", "::call:: " + call);
            // Response<TeamsList> response =
            call.execute();
            Log.i("CALL 2", ":::CALL::: " + call);
            // if (response.errorBody() != null){
            //    throw  defaultError();
            // }else{
            //      return response.body();
            // }
        } catch (IOException e) {
            throw defaultError();
        }
        return new TeamsList();
    }



    private IOException defaultError(){
        return new IOException("Ha ocurrido un error");
    }


}
