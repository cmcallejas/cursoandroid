package com.ccallejas.soccerleagues.views.interfaces;

import com.ccallejas.soccerleagues.models.TeamsList;

public interface ITeamsView extends IBaseView{

    void getTeams(TeamsList teamsList);

    void showAlert();
}
