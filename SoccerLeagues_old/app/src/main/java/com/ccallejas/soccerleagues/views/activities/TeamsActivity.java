package com.ccallejas.soccerleagues.views.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.ccallejas.soccerleagues.R;
import com.ccallejas.soccerleagues.adapter.AdapterTeams;
import com.ccallejas.soccerleagues.models.TeamsList;
import com.ccallejas.soccerleagues.presenters.TeamsPresenter;
import com.ccallejas.soccerleagues.views.interfaces.ITeamsView;

public class TeamsActivity extends BaseActivity<TeamsPresenter> implements ITeamsView {

    private String midLiga;
    private RecyclerView recyclerView_team;
    private AdapterTeams adapterTeams;
    LinearLayoutManager mlinearLayoutManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equipos);
        recyclerView_team = findViewById(R.id.recyclerViewEquipo);
        setPresenter(new TeamsPresenter());
        getPresenter().inject(this, getValidateInternet());
        midLiga = (String) getIntent().getSerializableExtra("idLiga");
        getPresenter().getListTeams(midLiga);

    }

    @Override
    public void getTeams(TeamsList teamsList) {
        thisTeamsActivity(teamsList);
       // finish();
    }

    private void thisTeamsActivity(final TeamsList teamsList) {

        runOnUiThread(new Runnable() {

            @Override
            public void run() {

                adapterTeams = new AdapterTeams(teamsList.getTeams(), TeamsActivity.this);
                mlinearLayoutManager = new LinearLayoutManager(TeamsActivity.this);
                recyclerView_team.setLayoutManager(mlinearLayoutManager);
                recyclerView_team.setAdapter(adapterTeams);

            }
        });


    }

    @Override
    public void showAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(R.string.title_validate_internet);
        alertDialog.setMessage(R.string.message_validate_internet);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(R.string.text_again, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
               // getPresenter().getListTeams();
                dialogInterface.dismiss();
            }
        });
        alertDialog.show();

    }


}
