package com.ccallejas.soccerleagues.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.ccallejas.soccerleagues.R;
import com.ccallejas.soccerleagues.models.Team;
import com.ccallejas.soccerleagues.views.interfaces.IActivityView;
import com.ccallejas.soccerleagues.views.interfaces.ITeamsView;

import java.util.ArrayList;

public class AdapterTeams extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Team> listaTeam;
    private ITeamsView miActivityView;

    public AdapterTeams(ArrayList<Team> listaTeam, ITeamsView miActivityView) {
        this.listaTeam = listaTeam;
        this.miActivityView = miActivityView;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_teams,
                viewGroup, false);
        return new  AdapterTeams.CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        AdapterTeams.CustomViewHolder customViewHolder = (CustomViewHolder) viewHolder;
        final Team team = listaTeam.get(position);
        customViewHolder.tvstrStadium.setText(team.getStrStadium());
        customViewHolder.tvStrTeam.setText(team.getStrTeam());
        //customViewHolder.imageStrTeamBadge.set
        customViewHolder.cardViewTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return (listaTeam !=null ?listaTeam.size():0);
    }

    private class CustomViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageStrTeamBadge;
        private TextView tvstrAlternate;
        private TextView tvStrTeam;
        private TextView tvstrStadium;
        private CardView cardViewTeam;

        CustomViewHolder(@NonNull View itemView) {
            super(itemView);
            imageStrTeamBadge = itemView.findViewById(R.id.imageViewLeague);
            tvStrTeam = itemView.findViewById(R.id.tvStrTeam);
            tvstrStadium = itemView.findViewById(R.id.tvstrStadium);
            cardViewTeam = itemView.findViewById(R.id.cardViewTeam);
        }

    }
}
