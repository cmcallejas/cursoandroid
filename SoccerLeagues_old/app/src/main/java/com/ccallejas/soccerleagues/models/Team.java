package com.ccallejas.soccerleagues.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class Team implements Serializable {


    public String getIdTeam() {
        return idTeam;
    }

    public void setIdTeam(String idTeam) {
        this.idTeam = idTeam;
    }

    public String getIdSoccerXML() {
        return idSoccerXML;
    }

    public void setIdSoccerXML(String idSoccerXML) {
        this.idSoccerXML = idSoccerXML;
    }

    public String getStrTeam() {
        return strTeam;
    }

    public void setStrTeam(String strTeam) {
        this.strTeam = strTeam;
    }

    public String getStrAlternate() {
        return strAlternate;
    }

    public void setStrAlternate(String strAlternate) {
        this.strAlternate = strAlternate;
    }

    public String getIntFormedYear() {
        return intFormedYear;
    }

    public void setIntFormedYear(String intFormedYear) {
        this.intFormedYear = intFormedYear;
    }

    public String getStrSport() {
        return strSport;
    }

    public void setStrSport(String strSport) {
        this.strSport = strSport;
    }

    public String getStrLeague() {
        return strLeague;
    }

    public void setStrLeague(String strLeague) {
        this.strLeague = strLeague;
    }

    public String getIdLeague() {
        return idLeague;
    }

    public void setIdLeague(String idLeague) {
        this.idLeague = idLeague;
    }

    public String getStrManager() {
        return strManager;
    }

    public void setStrManager(String strManager) {
        this.strManager = strManager;
    }

    public String getStrStadium() {
        return strStadium;
    }

    public void setStrStadium(String strStadium) {
        this.strStadium = strStadium;
    }

    public String getStrStadiumDescription() {
        return strStadiumDescription;
    }

    public void setStrStadiumDescription(String strStadiumDescription) {
        this.strStadiumDescription = strStadiumDescription;
    }

    public String getStrStadiumLocation() {
        return strStadiumLocation;
    }

    public void setStrStadiumLocation(String strStadiumLocation) {
        this.strStadiumLocation = strStadiumLocation;
    }

    public String getIntStadiumCapacity() {
        return intStadiumCapacity;
    }

    public void setIntStadiumCapacity(String intStadiumCapacity) {
        this.intStadiumCapacity = intStadiumCapacity;
    }

    public String getStrWebsite() {
        return strWebsite;
    }

    public void setStrWebsite(String strWebsite) {
        this.strWebsite = strWebsite;
    }

    public String getStrDescriptionEN() {
        return strDescriptionEN;
    }

    public void setStrDescriptionEN(String strDescriptionEN) {
        this.strDescriptionEN = strDescriptionEN;
    }

    public String getStrDescriptionES() {
        return strDescriptionES;
    }

    public void setStrDescriptionES(String strDescriptionES) {
        this.strDescriptionES = strDescriptionES;
    }

    public String getStrGender() {
        return strGender;
    }

    public void setStrGender(String strGender) {
        this.strGender = strGender;
    }

    public String getStrCountry() {
        return strCountry;
    }

    public void setStrCountry(String strCountry) {
        this.strCountry = strCountry;
    }

    public String getStrTeamBadge() {
        return strTeamBadge;
    }

    public void setStrTeamBadge(String strTeamBadge) {
        this.strTeamBadge = strTeamBadge;
    }

    public String getStrTeamJersey() {
        return strTeamJersey;
    }

    public void setStrTeamJersey(String strTeamJersey) {
        this.strTeamJersey = strTeamJersey;
    }

    public String getStrTeamLogo() {
        return strTeamLogo;
    }

    public void setStrTeamLogo(String strTeamLogo) {
        this.strTeamLogo = strTeamLogo;
    }

    public String getStrTeamFanart1() {
        return strTeamFanart1;
    }

    public void setStrTeamFanart1(String strTeamFanart1) {
        this.strTeamFanart1 = strTeamFanart1;
    }

    public String getStrTeamFanart2() {
        return strTeamFanart2;
    }

    public void setStrTeamFanart2(String strTeamFanart2) {
        this.strTeamFanart2 = strTeamFanart2;
    }

    public String getStrTeamFanart3() {
        return strTeamFanart3;
    }

    public void setStrTeamFanart3(String strTeamFanart3) {
        this.strTeamFanart3 = strTeamFanart3;
    }

    public String getStrTeamFanart4() {
        return strTeamFanart4;
    }

    public void setStrTeamFanart4(String strTeamFanart4) {
        this.strTeamFanart4 = strTeamFanart4;
    }

    public String getStrTeamBanner() {
        return strTeamBanner;
    }

    public void setStrTeamBanner(String strTeamBanner) {
        this.strTeamBanner = strTeamBanner;
    }


    @SerializedName("idTeam")
    @Expose
    private String idTeam;

    @SerializedName("idSoccerXML")
    @Expose
    private String idSoccerXML;

    @SerializedName("strTeam")
    @Expose
    private String strTeam;

    @SerializedName("strAlternate")
    @Expose
    private String strAlternate;

    @SerializedName("intFormedYear")
    @Expose
    private String intFormedYear;

    @SerializedName("strSport")
    @Expose
    private String strSport;

    @SerializedName("strLeague")
    @Expose
    private String strLeague;

    @SerializedName("idLeague")
    @Expose
    private String idLeague;

    @SerializedName("strManager")
    @Expose
    private String strManager;

    @SerializedName("strStadium")
    @Expose
    private String strStadium;

    @SerializedName("strStadiumDescription")
    @Expose
    private String strStadiumDescription ;

    @SerializedName("strStadiumLocation")
    @Expose
    private String strStadiumLocation;

    @SerializedName("intStadiumCapacity")
    @Expose
    private String intStadiumCapacity;

    @SerializedName("strWebsite")
    @Expose
    private String strWebsite;

    @SerializedName("strDescriptionEN")
    @Expose
    private String strDescriptionEN;

    @SerializedName("strDescriptionES")
    @Expose
    private String strDescriptionES;

    @SerializedName("strGender")
    @Expose
    private String strGender;

    @SerializedName("strCountry")
    @Expose
    private String strCountry;

    @SerializedName("strTeamBadge")
    @Expose
    private String strTeamBadge;

    @SerializedName("strTeamJersey")
    @Expose
    private String strTeamJersey;

    @SerializedName("strTeamLogo")
    @Expose
    private String strTeamLogo;

    @SerializedName("strTeamFanart1")
    @Expose
    private String strTeamFanart1;

    @SerializedName("strTeamFanart2")
    @Expose
    private String strTeamFanart2;

    @SerializedName("strTeamFanart3")
    @Expose
    private String strTeamFanart3;

    @SerializedName("strTeamFanart4")
    @Expose
    private String strTeamFanart4;

    @SerializedName("strTeamBanner")
    @Expose
    private String strTeamBanner;
}
