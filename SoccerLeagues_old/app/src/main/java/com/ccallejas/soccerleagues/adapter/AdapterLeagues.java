package com.ccallejas.soccerleagues.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.ccallejas.soccerleagues.R;
import com.ccallejas.soccerleagues.models.Leagues;
import com.ccallejas.soccerleagues.models.TeamsList;
import com.ccallejas.soccerleagues.presenters.TeamsPresenter;
import com.ccallejas.soccerleagues.views.activities.TeamsActivity;
import com.ccallejas.soccerleagues.views.interfaces.IActivityView;

import java.util.ArrayList;


public class AdapterLeagues  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Leagues> listaLeagues;
    private Context context;
    private IActivityView miActivityView;

    public AdapterLeagues(ArrayList<Leagues> leages, IActivityView iActivityView) {
        this.listaLeagues = leages;
        this.miActivityView = iActivityView;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_leagues,
                viewGroup, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        AdapterLeagues.CustomViewHolder customViewHolder = (CustomViewHolder) viewHolder;
        final Leagues leagues = listaLeagues.get(position);
        customViewHolder.tvStrLeague.setText(leagues.getStrLeague());
        customViewHolder.tvStrLeagueAlternate.setText(leagues.getStrSport());
        customViewHolder.cardViewLiga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                miActivityView.getListTeams();
                //Intent intent = new Intent(context, TeamsActivity.class);
               // intent.putExtra("idLiga", leagues.getIdLeague());
                //context.getApplicationContext().startActivity(intent);
            }
        });
        //Picasso.get().load(leagues.getEscudo()).into(customViewHolder.imageView_Shield);
    }

    @Override
    public int getItemCount() {
        return listaLeagues.size();
    }


    private class CustomViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageViewLeague;
        private TextView tvStrLeague;
        private TextView tvStrLeagueAlternate;
        private CardView cardViewLiga;

        CustomViewHolder(@NonNull View itemView) {
            super(itemView);
            imageViewLeague = itemView.findViewById(R.id.imageViewLeague);
            tvStrLeague = itemView.findViewById(R.id.tvStrLeague);
            tvStrLeagueAlternate = itemView.findViewById(R.id.tvStrLeagueAlternate);
            cardViewLiga = itemView.findViewById(R.id.cardViewLeagues);
        }

    }
}
