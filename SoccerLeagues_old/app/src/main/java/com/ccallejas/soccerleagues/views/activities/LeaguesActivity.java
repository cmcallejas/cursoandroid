package com.ccallejas.soccerleagues.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.ccallejas.soccerleagues.R;
import com.ccallejas.soccerleagues.adapter.AdapterLeagues;
import com.ccallejas.soccerleagues.models.Listleagues;
import com.ccallejas.soccerleagues.models.TeamsList;
import com.ccallejas.soccerleagues.presenters.LeaguesPresenter;
import com.ccallejas.soccerleagues.views.interfaces.IActivityView;
import com.ccallejas.soccerleagues.views.interfaces.ILeaguesView;


public class LeaguesActivity extends BaseActivity<LeaguesPresenter> implements IActivityView {

    private Listleagues ligas;

    private RecyclerView recyclerView_ligas;
    private AdapterLeagues adapterLeagues;
    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leagues);
        recyclerView_ligas = findViewById(R.id.recyclerView);
        setPresenter(new LeaguesPresenter());
        getPresenter().inject(LeaguesActivity.this, getValidateInternet());
        ligas = (Listleagues) getIntent().getSerializableExtra("objectLigas");
        showLeaguesList(ligas);

    }


    private void showLeaguesList(Listleagues listleagues) {
        adapterLeagues = new AdapterLeagues(listleagues.getListaLeagues(), this);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView_ligas.setLayoutManager(linearLayoutManager);
        recyclerView_ligas.setAdapter(adapterLeagues);
    }


    @Override
    public void getListLeagues(Listleagues listleagues) {

    }

    @Override
    public void getListTeams() {
        Intent intent = new Intent(this, TeamsActivity.class);
        //intent.putExtra("idLiga", leagues.getIdLeague());
        startActivity(intent);
    }

    @Override
    public void showAlert() {

    }
}
