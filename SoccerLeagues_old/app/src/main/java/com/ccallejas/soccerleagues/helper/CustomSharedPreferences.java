package com.ccallejas.soccerleagues.helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.ccallejas.soccerleagues.models.Team;
import com.google.gson.Gson;


public class CustomSharedPreferences {

    private SharedPreferences sharedPreferences;

    public CustomSharedPreferences(Context context) {

        sharedPreferences =  context.getSharedPreferences("my_preferences", Context.MODE_PRIVATE);
    }


    public String getString(String key){
        if (sharedPreferences.contains(key)){
            return sharedPreferences.getString(key, null);
        }
        return null;
    }

    public void addString(String key, String value){
        if (value != null && key != null){
            addValue(key, value);
        }
    }

    public void saveEquipo(String key, Team user){
        Gson gson = new Gson();
        String jsonUser =  gson.toJson(user);
        addValue(key, jsonUser);
    }

    public Team getEquipos(String key){
        Gson gson = new Gson();
        String jsonUser = sharedPreferences.getString(key, null);
        Team team = gson.fromJson(jsonUser, Team.class);
        return team;
    }

    private void addValue(String key, String value) {
        sharedPreferences.edit().putString(key, value).apply();
    }

    public void deleteValue(String key){
        sharedPreferences.edit().remove(key).apply();
    }
}


