package com.ccallejas.soccerleagues.views.interfaces;


import com.ccallejas.soccerleagues.models.Listleagues;
import com.ccallejas.soccerleagues.models.TeamsList;

public interface IActivityView extends IBaseView{

    //void getListTeams(TeamsList teamsList);
    void getListLeagues(Listleagues listleagues);
    void getListTeams();

    void showAlert();
}
