package com.ccallejas.soccerleagues.presenters;

import com.ccallejas.soccerleagues.helper.ValidateInternet;
import com.ccallejas.soccerleagues.views.interfaces.IBaseView;

public class BasePresenter<T extends IBaseView>{

    private ValidateInternet validateInternet;
    private T view;

    public void inject(T view, ValidateInternet _validateInternet){
        this.validateInternet = _validateInternet;
        this.view = view;
    }

    public ValidateInternet getValidateInternet() {
        return validateInternet;
    }

    public T getView() {
        return view;
    }
}
