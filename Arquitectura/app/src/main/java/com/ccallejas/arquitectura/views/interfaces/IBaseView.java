package com.ccallejas.arquitectura.views.interfaces;

public interface IBaseView {

     void showToast(int result);

}
