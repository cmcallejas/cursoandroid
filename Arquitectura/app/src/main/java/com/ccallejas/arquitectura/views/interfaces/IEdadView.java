package com.ccallejas.arquitectura.views.interfaces;

public interface IEdadView extends IBaseView  {

    void getEdad(int edad);
}
