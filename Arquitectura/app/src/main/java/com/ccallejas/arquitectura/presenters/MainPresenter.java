package com.ccallejas.arquitectura.presenters;

import com.ccallejas.arquitectura.views.interfaces.IMainView;

public class MainPresenter  extends  BasePresenter <IMainView>{

    // opción 1
    public int calculate(int one,int two){
        return one + two;
    }

    // opción 2
    public void calculate_two(int one,int two){
       getView().showResult(one + two);
    }

    // opción 3
    public void calculate_three(int i, int i1) {
        getView().showToast(i+i1);
    }
}
