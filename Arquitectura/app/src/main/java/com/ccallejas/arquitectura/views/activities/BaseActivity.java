package com.ccallejas.arquitectura.views.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.ccallejas.arquitectura.helper.ValidateInternet;
import com.ccallejas.arquitectura.presenters.BasePresenter;
import com.ccallejas.arquitectura.views.interfaces.IBaseView;

public class BaseActivity  <T extends BasePresenter> extends AppCompatActivity implements IBaseView {

    private ValidateInternet validateInternet;
    private T Presenter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        validateInternet = new ValidateInternet(this);
    }

    public ValidateInternet getValidateInternet() {
        return validateInternet;
    }

    public void setValidateInternet(ValidateInternet validateInternet) {
        this.validateInternet = validateInternet;
    }



    public T getPresenter() {
        return Presenter;
    }

    public void setPresenter(T presenter) {
        Presenter = presenter;
    }


    @Override
    public void showToast(int result) {
        Toast.makeText(this, String.valueOf(result), Toast.LENGTH_SHORT).show();
    }
}
