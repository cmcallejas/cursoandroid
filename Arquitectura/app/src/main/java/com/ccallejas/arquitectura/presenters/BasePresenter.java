package com.ccallejas.arquitectura.presenters;

import com.ccallejas.arquitectura.helper.ValidateInternet;
import com.ccallejas.arquitectura.views.interfaces.IBaseView;

public class BasePresenter  <T extends IBaseView>{

    private ValidateInternet validateInternet;
    private T view;

    public void inject(T view, ValidateInternet _validateInternet){
        this.validateInternet = _validateInternet;
        this.view = view;
    }

    public ValidateInternet getValidateInternet() {
        return validateInternet;
    }

    public T getView() {
        return view;
    }
}
