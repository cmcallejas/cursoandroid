package com.ccallejas.arquitectura.views.interfaces;

public interface IMainView  extends IBaseView {

    void showResult(int i);
}

