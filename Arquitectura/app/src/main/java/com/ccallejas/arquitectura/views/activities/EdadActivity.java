package com.ccallejas.arquitectura.views.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.ccallejas.arquitectura.R;
import com.ccallejas.arquitectura.presenters.EdadPresenter;
import com.ccallejas.arquitectura.views.interfaces.IEdadView;

public class EdadActivity extends BaseActivity<EdadPresenter>  implements IEdadView {

    private EditText anioNacimiento;
    private EditText anioActual;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edad_layout);
        setComponents();
        setPresenter(new EdadPresenter());
        getPresenter().inject(this,getValidateInternet());
    }

    private void setComponents() {
        anioNacimiento = findViewById(R.id.etAnioNacimiento );
        anioActual = findViewById(R.id.etAnioActual);
    }

    public void calcularEdad(View view) {
        getPresenter().calcularEdad(Integer.parseInt(anioNacimiento.getText().toString()),Integer.parseInt(anioActual.getText().toString()));
    }

    @Override
    public void getEdad(int edad) {
        Toast.makeText(this, String.valueOf(edad), Toast.LENGTH_SHORT).show();
    }


}
