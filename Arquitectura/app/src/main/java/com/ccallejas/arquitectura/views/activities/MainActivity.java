package com.ccallejas.arquitectura.views.activities;

import android.support.annotation.Nullable;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.ccallejas.arquitectura.R;
import com.ccallejas.arquitectura.presenters.MainPresenter;
import com.ccallejas.arquitectura.views.interfaces.IMainView;

public class MainActivity extends BaseActivity<MainPresenter> implements IMainView {

    private EditText numbreOne;
    private EditText numbreTwo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setComponents();
        setPresenter(new MainPresenter());
        getPresenter().inject(this,getValidateInternet());
    }

    private void setComponents() {
        numbreOne = findViewById(R.id.et1);
        numbreTwo = findViewById(R.id.et2);
    }

    public void calculate(View view) {
       //opcion 1
        //int result = getPresenter().calculate(Integer.parseInt(numbreOne.getText().toString()),Integer.parseInt(numbreTwo.getText().toString()));
       // Toast.makeText(this, String.valueOf(result), Toast.LENGTH_SHORT).show();

        // opcion 2
        getPresenter().calculate_two(Integer.parseInt(numbreOne.getText().toString()),Integer.parseInt(numbreTwo.getText().toString()));

        // // opcion 3
        getPresenter().calculate_three(Integer.parseInt(numbreOne.getText().toString()),Integer.parseInt(numbreTwo.getText().toString()));
    }


    //opcion 2
    @Override
    public void showResult(int i) {
        Toast.makeText(this, String.valueOf(i), Toast.LENGTH_SHORT).show();
    }

}
