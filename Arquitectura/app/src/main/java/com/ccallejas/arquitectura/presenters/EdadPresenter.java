package com.ccallejas.arquitectura.presenters;

import com.ccallejas.arquitectura.views.interfaces.IEdadView;

public class EdadPresenter extends  BasePresenter <IEdadView> {

    public void calcularEdad(int anioNacimiento, int anioActual){
        getView().getEdad(anioActual - anioNacimiento);
    }
}
