package com.ccallejas.soccerleagues.views.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import com.ccallejas.soccerleagues.R;
import com.ccallejas.soccerleagues.helper.Constants;
import com.ccallejas.soccerleagues.models.Listleagues;
import com.ccallejas.soccerleagues.presenters.MainPresenter;
import com.ccallejas.soccerleagues.views.interfaces.IActivityView;


public class MainActivity extends BaseActivity<MainPresenter> implements IActivityView {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setPresenter(new MainPresenter());
        getPresenter().inject(this, getValidateInternet());
        getPresenter().getListLigas();
    }

    @Override
    public void getListLeagues(Listleagues listligas) {
        intentOtherActivity(listligas);
        finish();
    }

    private void intentOtherActivity(Listleagues listligas) {
        Intent intent = new Intent(this, LeaguesActivity.class);
        intent.putExtra(Constants.LISTA_LIGAS, listligas);
        startActivity(intent);
    }

    @Override
    public void showAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(R.string.title_validate_internet);
        alertDialog.setMessage(R.string.message_validate_internet);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(R.string.text_again, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getPresenter().getListLigas();
                dialogInterface.dismiss();
            }
        });
        alertDialog.show();
    }
}
