package com.ccallejas.soccerleagues.presenters;

import com.ccallejas.soccerleagues.models.ListEvents;
import com.ccallejas.soccerleagues.models.TeamsList;
import com.ccallejas.soccerleagues.services.Repository;
import com.ccallejas.soccerleagues.views.interfaces.IEventsView;

import java.io.IOException;


public class EventsPresenter extends  BasePresenter <IEventsView> {
    private Repository repository;

    public void getEventosTeam(String idteam) {
        if (getValidateInternet().isConnected()) {
            repository = new Repository();
            createThreadGetEvents(String.valueOf(idteam));
        } else {
            getView().showAlert(idteam);
        }

    }

    public void createThreadGetEvents(final String idteam){
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                getEventsTeams(idteam);
            }
        });
        thread.start();
    }


    private void getEventsTeams(String idTeam) {
        try {
            ListEvents listEvents = repository.getListaEventos(idTeam);
            getView().getEventos(listEvents.getEvents());
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }
}
