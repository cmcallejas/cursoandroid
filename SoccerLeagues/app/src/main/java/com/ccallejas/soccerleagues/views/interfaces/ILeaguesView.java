package com.ccallejas.soccerleagues.views.interfaces;


import com.ccallejas.soccerleagues.models.Leagues;

public interface ILeaguesView extends IBaseView{

    void intentToListTeam(Leagues leagues);

}
