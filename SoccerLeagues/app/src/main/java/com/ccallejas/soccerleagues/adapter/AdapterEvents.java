package com.ccallejas.soccerleagues.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.ccallejas.soccerleagues.R;
import com.ccallejas.soccerleagues.models.Events;
import com.ccallejas.soccerleagues.models.Team;
import com.ccallejas.soccerleagues.views.activities.EventsActivity;
import com.ccallejas.soccerleagues.views.interfaces.IEventsView;



import java.util.ArrayList;

public class AdapterEvents extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Events> mlistaEvents;
    private IEventsView miActivityView;
    private Team mteam;

    public AdapterEvents(ArrayList<Events> listaEvent, Team team,IEventsView miIEventsView) {
        this.mteam = team;
        this.mlistaEvents = listaEvent;
        this.miActivityView = miIEventsView;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_team_descripcion,
                viewGroup, false);
        return new  AdapterEvents.CustomViewHolderEventos(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        AdapterEvents.CustomViewHolderEventos customViewHolderEventos = (CustomViewHolderEventos) viewHolder;
        final Events events = mlistaEvents.get(position);

        mteam = ((EventsActivity) miActivityView).getTeam();

        customViewHolderEventos.tvStrEventos.setText(events.getStrEvent());

        //customViewHolderEventos.strFilename.setText(events.getStrFilename());
        customViewHolderEventos.txtstrLeague.setText("Liga: "+events.getStrLeague());
        customViewHolderEventos.tvstrDate.setText("Fecha: "+((null != events.getStrDate())&&(!"".equals(events.getStrDate()))?events.getStrDate():"Sin fecha"));
        customViewHolderEventos.txtstrTime.setText("Hora: "+((null != events.getStrTime())&&(!"".equals(events.getStrTime()))?events.getStrTime():"Sin hora"));

        customViewHolderEventos.cardViewEventos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return (mlistaEvents !=null ?mlistaEvents.size():0);
    }

    private class CustomViewHolderEventos extends RecyclerView.ViewHolder {

        private TextView tvStrEventos;
        private TextView strFilename;
        private TextView txtstrLeague;
        private TextView tvstrDate;
        private TextView txtstrTime;

        private CardView cardViewEventos;

        CustomViewHolderEventos(@NonNull View itemView) {
            super(itemView);
            tvStrEventos = itemView.findViewById(R.id.tvStrEventos);
            txtstrLeague =itemView.findViewById(R.id.txtstrLeague);
            tvstrDate =itemView.findViewById(R.id.tvstrDate);
            txtstrTime =itemView.findViewById(R.id.txtstrTime);

            cardViewEventos = itemView.findViewById(R.id.cardViewEventos);
        }

    }
}
