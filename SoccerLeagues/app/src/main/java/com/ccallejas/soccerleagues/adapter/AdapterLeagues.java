package com.ccallejas.soccerleagues.adapter;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.ccallejas.soccerleagues.R;
import com.ccallejas.soccerleagues.models.Leagues;
import com.ccallejas.soccerleagues.views.interfaces.ILeaguesView;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

public class AdapterLeagues  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Leagues> listaLeagues;
    private ILeaguesView miActivityView;

    public AdapterLeagues(ArrayList<Leagues> leages, ILeaguesView iActivityView) {
        this.listaLeagues = leages;
        this.miActivityView = iActivityView;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_leagues,
                viewGroup, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        AdapterLeagues.CustomViewHolder customViewHolder = (CustomViewHolder) viewHolder;
        final Leagues leagues = listaLeagues.get(position);
        customViewHolder.tvStrLeague.setText(leagues.getStrLeague());
        customViewHolder.tvStrLeagueAlternate.setText(leagues.getStrLeagueAlternate());
        customViewHolder.tvStrSport.setText("Deporte: "+leagues.getStrSport());
        //Picasso.get().load("@drawable/ico"+leagues.getIdLeague().trim()).into(customViewHolder.imageViewLeague);
        //Uri uri = Uri.parse("@drawable/ico"+leagues.getIdLeague().trim());


        //Picasso.get().load(R.drawable.ico4328).into(customViewHolder.imageViewLeague);

        customViewHolder.cardViewLiga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                miActivityView.intentToListTeam(leagues);

            }
        });
        //Picasso.get().load(leagues.getEscudo()).into(customViewHolder.imageView_Shield);
    }

    @Override
    public int getItemCount() {
        return listaLeagues.size();
    }


    private class CustomViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageViewLeague;
        private TextView tvStrLeague;
        private TextView tvStrLeagueAlternate;
        private CardView cardViewLiga;
        private TextView tvStrSport;

        CustomViewHolder(@NonNull View itemView) {
            super(itemView);
            imageViewLeague = itemView.findViewById(R.id.imageViewLeague);
            tvStrLeague = itemView.findViewById(R.id.tvStrLeague);
            tvStrLeagueAlternate = itemView.findViewById(R.id.tvStrLeagueAlternate);
            tvStrSport = itemView.findViewById(R.id.tvStrSport);
            cardViewLiga = itemView.findViewById(R.id.cardViewLeagues);
        }

    }
}
