package com.ccallejas.soccerleagues.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;

public class ListEvents implements Serializable {

    @SerializedName("events")
    @Expose
    ArrayList<Events> events;

    public ArrayList<Events> getEvents() {
        return events;
    }

    public void setEvents(ArrayList<Events> events) {
        this.events = events;
    }
}
