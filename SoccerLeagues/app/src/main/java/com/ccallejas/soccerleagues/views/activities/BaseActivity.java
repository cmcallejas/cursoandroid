package com.ccallejas.soccerleagues.views.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;
import com.ccallejas.soccerleagues.helper.ValidateInternet;
import com.ccallejas.soccerleagues.presenters.BasePresenter;
import com.ccallejas.soccerleagues.views.interfaces.IBaseView;


public class BaseActivity<T extends BasePresenter> extends AppCompatActivity implements IBaseView {

    private ValidateInternet validateInternet;
    private T Presenter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        validateInternet = new ValidateInternet(this);
    }

    public ValidateInternet getValidateInternet() {
        return validateInternet;
    }

    public void setValidateInternet(ValidateInternet validateInternet) {
        this.validateInternet = validateInternet;
    }



    public T getPresenter() {
        return Presenter;
    }

    public void setPresenter(T presenter) {
        Presenter = presenter;
    }


    @Override
    public void showToast(int result) {
        Toast.makeText(this, String.valueOf(result), Toast.LENGTH_SHORT).show();
    }
}
