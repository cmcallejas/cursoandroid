package com.ccallejas.soccerleagues.views.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.ccallejas.soccerleagues.R;
import com.ccallejas.soccerleagues.adapter.AdapterTeams;
import com.ccallejas.soccerleagues.helper.Constants;
import com.ccallejas.soccerleagues.models.Leagues;
import com.ccallejas.soccerleagues.models.Team;
import com.ccallejas.soccerleagues.presenters.TeamsPresenter;
import com.ccallejas.soccerleagues.views.interfaces.ITeamsView;
import java.util.ArrayList;

public class TeamsActivity extends BaseActivity<TeamsPresenter> implements ITeamsView {

    private AdapterTeams adapterTeams;
    private Leagues leagues;
    private RecyclerView recyclerView_team;
    private TextView tvNombreLiga;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teams);
        recyclerView_team = findViewById(R.id.recyclerViewTeam);
        tvNombreLiga = findViewById(R.id.tvNombreLiga);
        setPresenter(new TeamsPresenter());
        getPresenter().inject(this, getValidateInternet());

        leagues = (Leagues) getIntent().getSerializableExtra(Constants.LIGA);
        getPresenter().getListTeams(leagues.getIdLeague());

    }

    @Override
    public void intentToListTeam(Team team) {
        Intent intent = new Intent(this, EventsActivity.class);
        intent.putExtra(Constants.TEAM, team);
        startActivity(intent);
    }

    @Override
    public void showAlert(final String idLiga) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(R.string.title_validate_internet);
        alertDialog.setMessage(R.string.message_validate_internet);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton(R.string.text_again, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getPresenter().getListTeams(idLiga);
                dialogInterface.dismiss();
            }
        });
        alertDialog.show();
    }

    @Override
    public void getTeams(final ArrayList<Team>  teamsList) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(TeamsActivity.this);
                recyclerView_team.setLayoutManager(linearLayoutManager);
                recyclerView_team.setHasFixedSize(true);
                adapterTeams = new AdapterTeams(teamsList, TeamsActivity.this);
                recyclerView_team.setAdapter(adapterTeams);
                tvNombreLiga.setText("Liga: "+leagues.getStrLeague());
            }
        });
    }




}
