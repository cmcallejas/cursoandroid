package com.ccallejas.soccerleagues.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.ccallejas.soccerleagues.R;
import com.ccallejas.soccerleagues.models.Team;
import com.ccallejas.soccerleagues.views.interfaces.ITeamsView;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

public class AdapterTeams extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Team> listaTeam;
    private ITeamsView miActivityView;

    public AdapterTeams(ArrayList<Team> listaTeam, ITeamsView miActivityView) {
        this.listaTeam = listaTeam;
        this.miActivityView = miActivityView;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_teams,
                viewGroup, false);
        return new  AdapterTeams.CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        AdapterTeams.CustomViewHolder customViewHolder = (CustomViewHolder) viewHolder;
        final Team team = listaTeam.get(position);
        customViewHolder.tvStrTeam.setText(team.getStrTeam());
        customViewHolder.tvstrStadium.setText(team.getStrStadium());
        customViewHolder.tvstrAlternate.setText(team.getStrAlternate());
        Picasso.get().load(team.getStrTeamBadge()).into(customViewHolder.imageStrTeamBadge);
        customViewHolder.cardViewTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(":::TEAM::: ","OBJETO TEAM :"+team);
                miActivityView.intentToListTeam(team);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (listaTeam !=null ?listaTeam.size():0);
    }

    private class CustomViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageStrTeamBadge;
        private TextView tvstrAlternate;
        private TextView tvStrTeam;
        private TextView tvstrStadium;
        private CardView cardViewTeam;

        CustomViewHolder(@NonNull View itemView) {
            super(itemView);
            imageStrTeamBadge = itemView.findViewById(R.id.imageViewTeam);
            tvStrTeam = itemView.findViewById(R.id.tvStrTeam);
            tvstrStadium = itemView.findViewById(R.id.tvstrStadium);
            tvstrAlternate =itemView.findViewById(R.id.strAlternate);
            cardViewTeam = itemView.findViewById(R.id.cardViewTeam);
        }

    }
}
