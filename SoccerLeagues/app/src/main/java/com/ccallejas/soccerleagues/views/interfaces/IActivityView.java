package com.ccallejas.soccerleagues.views.interfaces;


import com.ccallejas.soccerleagues.models.Listleagues;


public interface IActivityView extends IBaseView{

    void getListLeagues(Listleagues listleagues);
    void showAlert();
}
