package com.ccallejas.soccerleagues.helper;

public class Constants {

    public final static String TOKEN = "token";
    public static final String USER = "user";
    public static final String LISTA_LIGAS = "LISTA_LIGAS";
    public static final String LIGA = "LIGA";
    public static final String TEAM = "TEAM";
    public static final String EVENTOS = "EVENTOS";

}
